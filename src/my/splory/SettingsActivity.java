package my.splory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.parse.ParseUser;

public class SettingsActivity extends BaseActivity {
	private Context c;
	private TextView tv_logout;
	private static final String TAG = "SettingsActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
	
		tv_logout = (TextView)findViewById(R.id.tv_logout);
		
		tv_logout.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				ParseUser.logOut();
				Intent i = new Intent(SettingsActivity.this, LoginActivity.class);
				startActivity(i);
				finish();
			
			}
		});
	}
}
