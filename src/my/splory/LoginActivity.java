package my.splory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class LoginActivity extends Activity {
	private TextView tv_fb_login;
	private ProgressBar progressBar;
	private Boolean isSessionOpened;
	private Context c;
	private ParseFile imageFile;
	private static final String TAG = "LoginActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		c = this;
		tv_fb_login = (TextView) findViewById(R.id.tv_fb_login);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		tv_fb_login.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				progressBar.setVisibility(ProgressBar.VISIBLE);
				String[] permissions = getResources().getStringArray(R.array.fb_permissions);
				ParseFacebookUtils.logIn(Arrays.asList(permissions), LoginActivity.this,
						new LogInCallback() {
							@Override
							public void done(ParseUser user, ParseException err) {
								Log.i(TAG, "Calling done() at LogInCallback()");
								progressBar.setVisibility(ProgressBar.GONE);
								if (user == null) {
									Toast.makeText(c, "Please try again.", Toast.LENGTH_SHORT).show();									
								} else if (user.isNew()) {
									Toast.makeText(c, "Hi new user.", Toast.LENGTH_SHORT).show();	
									if (isSessionOpened()) {
										setAdditionalUserInfo();
									}
									Intent i = new Intent(LoginActivity.this,
											MainActivity.class);
									startActivity(i);
									finish();
								} else {
									Toast.makeText(c, "Hi existing user.", Toast.LENGTH_SHORT).show();	
									Log.i(TAG,"existing user " + user.getObjectId() + "" + user.getUsername());
									if (isSessionOpened()) {
										setAdditionalUserInfo();
									}
									Intent i = new Intent(LoginActivity.this,
											MainActivity.class);
									startActivity(i);
									finish();
								}
							}

						});
			}

		});

	}

	private Boolean isSessionOpened() {
		// Fetch Facebook user info if the session is active
		Session session = ParseFacebookUtils.getSession();
		if (session != null && session.isOpened()) {
			return true;
		}
		return false;
	}

	private void setAdditionalUserInfo() {		
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {			    
					@Override
					public void onCompleted(final GraphUser user, Response response) {
						Log.i(TAG, "Calling onCompleted() at GraphUserCallback()");
						if (user != null) {							
							//Start AsynTask to get profile picture from facebook
							 AsyncTask<String, Void, Bitmap> getFbImageAsyncTask = new AsyncTask<String, Void, Bitmap>(){
							    protected Bitmap doInBackground(String... urls) {
							    	Log.i(TAG, "Calling doInBackground() at AsyncTask");
							        Bitmap bitmap = null;
							        try {
							            InputStream inputStream = new URL(urls[0]).openStream();	          
							            bitmap = BitmapFactory.decodeStream(inputStream);	            
							        } catch (Exception e) {
							            Log.e(TAG, e.getMessage());
							            e.printStackTrace();
							        }
							        return bitmap;
							    }
							    protected void onPostExecute(Bitmap result) {
							    	Log.i(TAG, "Calling onPostExecute() at AsyncTask");
							    	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
							    	if(result!=null){							    
							    	result.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
							    	}
							    	byte[] byteArray = outputStream.toByteArray();
							    	imageFile = new ParseFile("profile.png", byteArray);
							    	imageFile.saveInBackground(new SaveCallback() {
							    		   public void done(ParseException e) {
							    			 Log.i(TAG, "Calling done() at onPostExecute()");
							    		     if (e == null) {
							    		    	// !Take note of nullpointerexception
							    		    	Date currentDate = new Date(System.currentTimeMillis());
							    		    	int initializedInt = 0;
							    		    	
							    		    	ParseUser currentUser = ParseUser.getCurrentUser();							    		    	
							    		    	currentUser.setEmail((String)user.getProperty("email"));							    		    	
												currentUser.put("birthday", user.getProperty("birthday"));
												currentUser.put("education",user.getProperty("education"));
												currentUser.put("fbuid", user.getId());
												currentUser.put("rank", "Newbie");
												currentUser.put("status","Hi. I'm on fire.");
												currentUser.put("followers_count",initializedInt);
												currentUser.put("buddies_count",initializedInt);
												currentUser.put("first_name", user.getFirstName());
												currentUser.put("last_name", user.getLastName());
												currentUser.put("middle_name", user.getMiddleName());
												currentUser.put("name", user.getName());
												currentUser.put("gender", user.getProperty("gender"));
												currentUser.put("link", user.getLink());
												currentUser.put("locale",user.getProperty("locale"));
												currentUser.put("location",user.getProperty("location"));
												currentUser.put("verified",user.getProperty("verified"));
												currentUser.put("work", user.getProperty("work"));
												currentUser.put("timezone", user.getProperty("timezone"));
												currentUser.put("hometown", user.getProperty("hometown"));
												currentUser.put("lastOpened", currentDate);
												currentUser.put("photo", imageFile);
												currentUser.saveInBackground(new SaveCallback(){
													public void done(ParseException e){
														if(e == null){
															Log.i(TAG, "User data successfully commited to Parse");
														}
														else{
															Log.i(TAG, "Fail to commit data on Parse." +e.getMessage());
														}
													}
												});
							    		     } else {
							    		    	 Log.d(TAG, e.getMessage());
							    		     }
							    		   }
							    		 });    	
							    }
							};		
							
							getFbImageAsyncTask.execute("https://graph.facebook.com/" + user.getId() + "/picture?type=large");
						
							
						} else if (response.getError() != null) {
							if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY)
									|| (response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
								Log.d(TAG,
										"The facebook session was invalidated.");
								// onLogoutButtonClicked();
							} else {
								Log.d(TAG, "Some other error: "
										+ response.getError().getErrorMessage());
							}
						}
					}
				});
		request.executeAsync();

	}

	// This will invoke the done() in LogInCallback() object.
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
}

// User log in
/*
 * ParseUser.logInInBackground(et_username.getText().toString(),
 * et_pw.getText().toString(), new LogInCallback() { public void done(ParseUser
 * user, ParseException e) { if (user != null) {
 * Toast.makeText(getApplicationContext(), "Successful login" ,
 * Toast.LENGTH_SHORT).show(); Intent i = new Intent(LoginActivity.this,
 * MainActivity.class); startActivity(i); } else { // Signup failed. Look at the
 * ParseException to see what happened. String msg=""; switch (e.getCode()) {
 * case ParseException.USERNAME_TAKEN: msg =
 * "Sorry, this username has already been taken."; break; case
 * ParseException.USERNAME_MISSING: msg =
 * "Sorry, you must supply a username to register."; break; case
 * ParseException.PASSWORD_MISSING: msg
 * ="Sorry, you must supply a password to register."; break; case
 * ParseException.OBJECT_NOT_FOUND: msg =
 * "Sorry, those credentials were invalid."; break; case
 * ParseException.CONNECTION_FAILED: msg =
 * "Internet connection was not found. Please see your connection settings.";
 * break; default: Log.d("Testing",e.getLocalizedMessage()); break; }
 * 
 * Toast.makeText(getApplicationContext(), msg , Toast.LENGTH_SHORT).show(); } }
 * });
 */

// User sign up
/*
 * SploryUser user = new SploryUser(); user.setUsername("yctest");
 * user.setPassword("yctest"); user.setEmail("yctest@example.com");
 * user.signUpInBackground(new SignUpCallback() { public void
 * done(ParseException e) { if (e == null) { Log.d("yc", "signup successful"); }
 * else { Log.d("yc", "signup failed"); } } });
 */