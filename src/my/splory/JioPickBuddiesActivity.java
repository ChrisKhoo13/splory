package my.splory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import my.splory.helper.PickBuddiesListViewArrayAdapter;
import my.splory.object.PickBuddiesListViewUser;
import my.splory.subclass.parse.User;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
//Adding selected name to an arraylist. on list view row clicked
public class JioPickBuddiesActivity extends BaseActivity{
	private static final String TAG = "JioPickBuddiesActivity";
	private EditText et_search_bud;
	private ImageView imgv_cross;
	private ListView listview;
	private ArrayList<PickBuddiesListViewUser> pickBuddiesListViewUsers;
	private PickBuddiesListViewArrayAdapter adapter;
	private Context context;
	private Button btn_done;
	private TextView tv_buddies_selected;
	private ArrayList<String> buddiesSelected;
	private ArrayList<User> searchResults = new ArrayList<User>();
	private ArrayList<User> selectedBuddies = new ArrayList<User>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_buddies);
		context = this;
		et_search_bud = (EditText)findViewById(R.id.et_search_bud);
		imgv_cross = (ImageView)findViewById(R.id.imgv_cross);
		listview = (ListView)findViewById(R.id.listview);		
		btn_done = (Button)findViewById(R.id.btn_done);		
		tv_buddies_selected = (TextView)findViewById(R.id.tv_buddies_selected);	
		 
		//lv_user.setItemsCanFocus(false);
		imgv_cross.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				et_search_bud.getText().clear();
			}
		});
		
		
		btn_done.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				ArrayList<String> objectIds = new ArrayList<String>();
				Intent intent = new Intent();  
                for(int i=0; i < selectedBuddies.size();i++){
                	objectIds.add(selectedBuddies.get(i).getObjectId());                	
                }              
                intent.putStringArrayListExtra("INVITEES",objectIds);
                setResult(RESULT_OK, intent);  
                finish();				
			}
		});
		
	
		//Search bug - it is parse query access issue / query bug / own bug
		//refine confusing naming
		//Is that alright to call 'SearchBuddies' function 
		et_search_bud.addTextChangedListener(new TextWatcher(){
			public void onTextChanged(CharSequence s, int start, int before, int count){				   			   
					Map<String, String> searchWordMap = new HashMap<String, String>();
					searchWordMap.put("search", et_search_bud.getText().toString());
					Log.i(TAG, "Calling SearchBuddies with: "+ et_search_bud.getText().toString());
					ParseCloud.callFunctionInBackground("SearchBuddies", searchWordMap, new FunctionCallback<Map<String, ArrayList<User>>>() {
								public void done(Map<String, ArrayList<User>> results, ParseException e) {
									if(!searchResults.isEmpty()){
										searchResults.clear();
									}	
								    //pickBuddiesListViewUsers = new ArrayList<PickBuddiesListViewUser>();										    
									if (e == null ) {										
										for (Map.Entry<String, ArrayList<User>> entry : results.entrySet()) {											
											//entry.getKey() - friends, non friends
											ArrayList<User> users = entry.getValue();											
												for (int i = 0; i < users.size(); i++) {
													searchResults.add(users.get(i));	
													Log.i(TAG, "Adding users to searchResults: "+ users.get(i).getName());
												}										
										}
										//adapter = new PickBuddiesListViewArrayAdapter(context, R.layout.listview_row_jio_buddies, pickBuddiesListViewUsers);	
										adapter = new PickBuddiesListViewArrayAdapter(context, R.layout.listview_row_jio_buddies, searchResults);
									} else {
										//Empty listview
										//adapter = new PickBuddiesListViewArrayAdapter(context, R.layout.listview_row_jio_buddies, pickBuddiesListViewUsers);
										adapter = new PickBuddiesListViewArrayAdapter(context, R.layout.listview_row_jio_buddies, searchResults);
										Toast.makeText(getApplicationContext(), "Problem popping list view", Toast.LENGTH_SHORT).show();
										Log.e(TAG, e.getMessage());
									}		
									listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);  
									listview.setAdapter(adapter);
								}
							});
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		listview.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				 //PickBuddiesListViewUser user = (PickBuddiesListViewUser)parent.getItemAtPosition(position);		
				  User userObj = (User)parent.getItemAtPosition(position);	
				  CheckBox chk = (CheckBox) view.findViewById(R.id.checkbox_search_buddies);
				  if (userObj.isSelected()) {
					  	selectedBuddies.remove(userObj);
					  	userObj.setSelected(false);
						chk.setChecked(false);
					} else {
						selectedBuddies.add(userObj);
						userObj.setSelected(true);
						chk.setChecked(true);
					}
					  //Update textview tv_buddies_selected
				 String display = "";
				  for (User loopUser : selectedBuddies) {					  
						  display += loopUser.getName() +"\n";						 
				  }
				  tv_buddies_selected.setText(display);
					
				adapter.notifyDataSetChanged();				 
				
			  }
			  });
	
	}
}
