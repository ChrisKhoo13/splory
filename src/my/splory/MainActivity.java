package my.splory;

import my.splory.helper.TabsPagerAdapter;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

public class MainActivity extends BaseActivity implements ActionBar.TabListener{
	private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    private String[] tabs;
    private static final String TAG = "MainActivity";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Adding tabs to action bar
		String[] tabs = getResources().getStringArray(R.array.tab_title);
        viewPager = (ViewPager) findViewById(R.id.pager);        
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager()); 
        viewPager.setAdapter(mAdapter);
        
        actionBar = getActionBar();      
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);        
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }        
       
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {         
            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);
            }         
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
         
            @Override
            public void onPageScrollStateChanged(int arg0) {}
        });
        
       
	}
	
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {	
        viewPager.setCurrentItem(tab.getPosition());
    }
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {}
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {}

}
