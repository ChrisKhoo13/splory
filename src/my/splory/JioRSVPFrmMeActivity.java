package my.splory;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class JioRSVPFrmMeActivity extends Activity{
	private TextView tv_invitees;
	private TextView tv_event;
	private TextView tv_time;
	private TextView tv_venue;
	private TextView tv_notes;
	private TextView tv_rsvp_count;
	private String inviteesStr;
	private ArrayList<String> invitees = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_rsvp_frm_me);
		tv_invitees = (TextView)findViewById(R.id.tv_invitees);
		tv_event = (TextView)findViewById(R.id.tv_event);
		tv_time = (TextView)findViewById(R.id.tv_time);
		tv_venue = (TextView)findViewById(R.id.tv_venue);
		tv_notes = (TextView)findViewById(R.id.tv_notes);
		tv_rsvp_count = (TextView)findViewById(R.id.tv_rsvp_count);
		
		Intent intent = getIntent();
		invitees = intent.getStringArrayListExtra("invitees");
		for(int i=0; i<invitees.size(); i++){
			inviteesStr += invitees.get(i)+", ";
		}
		tv_invitees.setText(inviteesStr);
	}
}
