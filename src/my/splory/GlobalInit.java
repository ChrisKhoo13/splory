package my.splory;

import my.splory.subclass.parse.Buzz;
import my.splory.subclass.parse.Comment;
import my.splory.subclass.parse.Post;
import my.splory.subclass.parse.User;
import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;

public class GlobalInit extends Application { 

    @Override public void onCreate() { 
        super.onCreate();
        ParseObject.registerSubclass(Buzz.class);
        ParseObject.registerSubclass(User.class);
        ParseObject.registerSubclass(Post.class);
        ParseObject.registerSubclass(Comment.class);
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
        ParseFacebookUtils.initialize(getString(R.string.fb_app_id));
    }
} 