package my.splory.fragment;

import my.splory.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ProfileRingFragment extends Fragment {
	private Context c;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		c = getActivity();
		View v = inflater.inflate(R.layout.fragment_placeholder, container,
				false);
		return v;
	}
}
