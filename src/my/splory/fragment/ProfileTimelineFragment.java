package my.splory.fragment;

import java.util.ArrayList;

import my.splory.R;
import my.splory.fragment.FeedsFragment.LoadFeeds;
import my.splory.helper.CustomArrayAdapter;
import my.splory.object.Feeds;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class ProfileTimelineFragment extends Fragment{
	private Context c;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		c = getActivity();				
		View v = inflater.inflate(R.layout.fragment_placeholder, container, false);
		return v;
	}
}
