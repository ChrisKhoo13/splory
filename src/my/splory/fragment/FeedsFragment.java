package my.splory.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import my.splory.R;
import my.splory.helper.CustomArrayAdapter;
import my.splory.object.CheckInFeed;
import my.splory.object.Feeds;
import my.splory.object.JioFeed;
import my.splory.object.PhotoFeed;
import my.splory.subclass.parse.Buzz;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

public class FeedsFragment extends Fragment {
	private Context c;	
	List<Feeds> feeds;
	private ListView listview;	
	private TextView tv_buzzcount;
	private TextView tv_buzzlist;
	private static final String TAG = "FeedsFragment";
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		c = getActivity();
		feeds = new ArrayList<Feeds>();
		
		View v = inflater.inflate(R.layout.fragment_feeds, container, false);
		listview = (ListView) v.findViewById(R.id.listview);		
		tv_buzzcount = (TextView) v.findViewById(R.id.tv_buzzcount);
		tv_buzzlist = (TextView) v.findViewById(R.id.tv_buzzlist);
		//tv_buzzcount.setText(getBuzzCount());
		//tv_buzzlist.setText(getBuzzlist());
	
		new LoadFeeds().execute();
		
		CustomArrayAdapter adapter = new CustomArrayAdapter(c, feeds);
		listview.setAdapter(adapter);	
		return v;
	}
		
	class LoadFeeds extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
		
		protected String doInBackground(String... arg0) {			
			int selection=0;
			try {
               //Add feeds from Parse to [feeds array list]. 
				switch (selection){
				case 0:
					for (int i = 0; i < 10; i++) {
						 
	                    if (i % 3 == 0){
	                    	feeds.add(new PhotoFeed("Chris Khoo", "3:15 pm", "Testing", "Photo"));
	                    }
	                    else if (i % 3 == 1){
	                    	feeds.add(new JioFeed("Ducky", "4.15 pm", "Haram", "Jio"));
	                    }
	                    else{
	                    	feeds.add(new CheckInFeed("Steve", "5:15 pm", "Somewhere", "Checkin"));
	                    }
	                }		
					break;
					
				case 1:
					feeds.add(new JioFeed("Ducky", "4.15 pm", "Haram", "Jio"));
					break;
					
				case 2:
					feeds.add(new PhotoFeed("Chris Khoo", "3:15 pm", "Testing", "Photo"));
					break;
					
				case 3:
					feeds.add(new CheckInFeed("Steve", "5:15 pm", "Somewhere", "Checkin"));
					break;
				}
 
            } catch (Exception e) {
                e.printStackTrace();
            }
			
			return null;
		}


	}
}


//Check if currentUser is null
/*SploryUser currentUser = SploryUser.getCurrentUser();
if (currentUser != null) {			
	Log.d("yc", "current user is not null");
} else {
	Log.d("yc", "current user is null");
}*/
