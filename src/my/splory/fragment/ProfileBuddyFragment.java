package my.splory.fragment;

import java.util.ArrayList;

import my.splory.R;
import my.splory.helper.ProfileBuddyListViewArrayAdapter;
import my.splory.subclass.parse.User;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseRelation;
import com.parse.ParseUser;

public class ProfileBuddyFragment extends Fragment {
	private Context c;
	private ListView lv_myfriends;
	private ArrayList<User> myBuddies = new ArrayList<User>();
	private Context context;
	private static final String TAG = "ProfileBuddyFragment";
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();				
		View v = inflater.inflate(R.layout.fragment_profile_buddy, container, false);
		lv_myfriends = (ListView)v.findViewById(R.id.lv_myfriends);		
		ProfileBuddyListViewArrayAdapter adapter = new ProfileBuddyListViewArrayAdapter(context, R.layout.listview_row_profile_buddy, getMyBuddies());
		lv_myfriends.setAdapter(adapter);
		return v;
	}

	private ArrayList<User> getMyBuddies() {
		User currentUser = (User) ParseUser.getCurrentUser();	
		ParseRelation<User> myBuddiesParseRel = currentUser.getFriends();
		try {
			for(int i=0 ; i<myBuddiesParseRel.getQuery().count() ; i++){
				myBuddies.add(myBuddiesParseRel.getQuery().find().get(i));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return myBuddies;
	}
	
	
}
