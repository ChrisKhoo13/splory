package my.splory.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import my.splory.JioComposeActivity;
import my.splory.JioRSVPFrmMeActivity;
import my.splory.JioRSVPFrmOthersActivity;
import my.splory.R;
import my.splory.helper.HangoutListViewArrayAdapter;
import my.splory.helper.GeneralFunctions;
import my.splory.subclass.parse.Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

public class JioFragment extends Fragment {
	private Context context;
	private ImageButton imgb_jio;
	private ListView lv_nx_hangout;
	private ListView lv_other_hangout;
	private ArrayList<Post> nextHangouts = new ArrayList<Post>();
	private ArrayList<Post> otherHangouts = new ArrayList<Post>();
	private ArrayList<String> inviteesObjId = new ArrayList<String>();
	private ArrayList<String> invitees = new ArrayList<String>();
	private static final String TAG = "JioFragment";		
	private static final String HANGOUT_UPCOMING = "upcoming";
	private static final String HANGOUT_OTHERS = "rsvped";		
	private static final String HANGOUT_RSVPED = "others";	
	public static final String HANGOUT_NAME = "hangout name";
	public static final String HANGOUT_FROM = "from";	
	public static final String HANGOUT_INVITEES = "invitees";	
	public static final String HANGOUT_TIME = "time";	
	public static final String HANGOUT_VENUE = "venue";	
	public static final String HANGOUT_NOTES = "notes";	
	public static final String HANGOUT_ID = "id";		
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();
		View view = inflater.inflate(R.layout.fragment_jio, container, false);
		imgb_jio = (ImageButton)view.findViewById(R.id.imgb_jio);
		lv_nx_hangout = (ListView)view.findViewById(R.id.lv_nx_hangout);
		lv_other_hangout = (ListView)view.findViewById(R.id.lv_other_hangout);
		
		imgb_jio.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				Intent i = new Intent(getActivity(),JioComposeActivity.class);
				startActivity(i);
			}
		});		
		
		lv_nx_hangout.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				  Post hangout = (Post) parent.getItemAtPosition(position);
				 
				  JSONArray inviteesJson = hangout.getBuddies();
				  for(int i=0; i<inviteesJson.length(); i++){					 
					try {
						  JSONObject obj = inviteesJson.getJSONObject(i);
						  inviteesObjId.add(obj.getString("objectId"));
					} catch (JSONException e) {
						e.printStackTrace();
					}					
				  }
				  GeneralFunctions func = new GeneralFunctions();
				  Log.i(TAG, "Obj id size: "+inviteesObjId.size());
				  //invitees = func.getParseUserName(inviteesObjId);
				  Log.i(TAG, "Returned invitees size: "+invitees.size());
				  String loginUserObjId = ParseUser.getCurrentUser().getObjectId();	
				  String postByObjId = hangout.getUserId().getObjectId();
				  //This hangout is initiated by me
				  if(postByObjId.equals(loginUserObjId)){
					  Intent intent = new Intent(getActivity(),JioRSVPFrmMeActivity.class);					  
					  intent.putExtra("event", hangout.getEventName());
					  intent.putExtra("time", func.formatDate(hangout.getDateFrom(), "dd MMM, ccc. h a"));
					  intent.putExtra("venue", func.getLocation(hangout.getLocation()));
					  intent.putExtra("notes", hangout.getContent());
					  intent.putStringArrayListExtra("invitees", invitees);
					  startActivity(intent);
					  Log.i(TAG, "Hangout created by me");
				  }
				//This hangout is initiated by others				
				  else{
					  Intent intent = new Intent(getActivity(),JioRSVPFrmOthersActivity.class);		
					  intent.putExtra(HANGOUT_FROM, hangout.getUserId().getName());					  
					  intent.putExtra(HANGOUT_NAME, hangout.getEventName());
					  intent.putExtra(HANGOUT_TIME, func.formatDate(hangout.getDateFrom(), "dd MMM, ccc. h a"));
					  intent.putExtra(HANGOUT_VENUE, func.getLocation(hangout.getLocation()));
					  intent.putExtra(HANGOUT_NOTES, hangout.getContent());		
					  intent.putStringArrayListExtra(HANGOUT_INVITEES, inviteesObjId);
					  startActivity(intent);
					  Log.i(TAG, "Hangout created by others");
				  }
				  
			  }});
		
		displayNextHangouts();
		return view;
	}
	
	private void displayNextHangouts() {
		Map<String, String> emptyMap = new HashMap<String, String>();		
		ParseCloud.callFunctionInBackground("GetHangouts", emptyMap, new FunctionCallback<Map<String, ArrayList<Post>>>() {
			@Override
			public void done(Map<String, ArrayList<Post>> results, ParseException e) {
				if (e == null ) {									
					for(Map.Entry<String, ArrayList<Post>> entry : results.entrySet()) {							
							if(entry.getKey().contains(HANGOUT_UPCOMING)){
								for( Post hangout: entry.getValue()){
									nextHangouts.add(hangout);
									Log.i(TAG, "upcoming hangout: "+hangout.getEventName());
								}
							}	
							else if(entry.getKey().contains(HANGOUT_OTHERS)){
								for( Post hangout: entry.getValue()){
									otherHangouts.add(hangout);
									Log.i(TAG, "other hangout: "+hangout.getEventName());
								}
							}
							else{
								//HANGOUT_RSVPED
								for( Post hangout: entry.getValue()){
									otherHangouts.add(hangout);
									Log.i(TAG, "rsvp hangout: "+hangout.getEventName());
								}								
							}
						}
				HangoutListViewArrayAdapter nextHangoutAdapter = new HangoutListViewArrayAdapter(context, R.layout.listview_row_hangout, nextHangouts);
				HangoutListViewArrayAdapter otherHangoutAdapter = new HangoutListViewArrayAdapter(context, R.layout.listview_row_hangout, otherHangouts);
				
				lv_nx_hangout.setAdapter(nextHangoutAdapter);
				//Need to be able to calculate the dynamic height of listview
				lv_nx_hangout.post(new Runnable(){
				    public void run(){
				    	Log.i(TAG, "children count: "+ Integer.toString(lv_nx_hangout.getChildCount()));						
				    	int height = 0;
						for (int i = 0; i < lv_nx_hangout.getChildCount(); i++) {
						    height += lv_nx_hangout.getChildAt(i).getMeasuredHeight();
						    height += lv_nx_hangout.getDividerHeight();
						    Log.i(TAG, "adding up height: "+ Integer.toString(height));
						}
						ViewGroup.LayoutParams params = lv_nx_hangout.getLayoutParams();
					    params.height = height*3;
					    params.width = LayoutParams.WRAP_CONTENT;
					    lv_nx_hangout.setLayoutParams(params);
				    }});
				  
				lv_other_hangout.setAdapter(otherHangoutAdapter);								
			}
			
				else{
					Toast.makeText(context, "ParseException at GetHangouts: "+e.getMessage(), Toast.LENGTH_LONG).show();
				}
				
		};
		});
		
}
}

/*FragmentTransaction trans = getFragmentManager()
.beginTransaction();

 //IMPORTANT: We use the "root frame" defined in
// "root_fragment.xml" as the reference to replace fragment

trans.replace(R.id.root_frame, new JioComposeFragment());


//IMPORTANT: The following lines allow us to add the fragment
//to the stack and return to it later, by pressing back

trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
trans.addToBackStack(null);

trans.commit();*/

/*et_search_friend = (EditText) view.findViewById(R.id.et_search_friend);
btn_search = (Button) view.findViewById(R.id.btn_search);
tv_friendlist = (TextView) view.findViewById(R.id.tv_friendlist);

btn_search.setOnClickListener(new OnClickListener() {
	public void onClick(View v) {

		Map<String, String> searchBuddiesMap = new HashMap<String, String>();
		searchBuddiesMap.put("search", et_search_friend.getText().toString());

		//Results are appended in str variable. Process the results accordingly later. 
		ParseCloud.callFunctionInBackground("SearchBuddies", searchBuddiesMap,
				new FunctionCallback<Map<String, ArrayList>>() {
					public void done(Map<String, ArrayList> results,
							ParseException e) {
						if (e == null) {
							String str = "";
							for (Map.Entry<String, ArrayList> entry : results.entrySet()) {
								str += entry.getKey() + ": ";
								ArrayList<ParseUser> name = entry.getValue();
								for (int i = 0; i < name.size(); i++) {
									str += name.get(i)
											.getString("name");
								}

							}
							tv_friendlist.setText(str);
						} else {
							Log.d(TAG, e.toString());
						}
					}
				});
	}

});*/



