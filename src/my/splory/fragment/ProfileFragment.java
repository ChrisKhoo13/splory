package my.splory.fragment;

import my.splory.R;
import my.splory.helper.PhotoHelper;
import my.splory.subclass.parse.User;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

public class ProfileFragment extends Fragment {
	private Context c;
	private static final String TAG = "ProfileFragment";
	private FragmentTabHost tabHost;
	private ImageView imgv_photo;
	private TextView tv_rank;
	private TextView tv_name;
	private TextView tv_status;
	private TextView tv_followers;
	private TextView tv_buddies;	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		c = getActivity();
		View v = inflater.inflate(R.layout.fragment_profile, container,
				false);		
		 tv_rank = (TextView)v.findViewById(R.id.tv_rank);
		 tv_name = (TextView)v.findViewById(R.id.tv_name);
		 tv_status = (TextView)v.findViewById(R.id.tv_status);
		 tv_followers = (TextView)v.findViewById(R.id.tv_followers);
		 tv_buddies = (TextView)v.findViewById(R.id.tv_buddies);
		 imgv_photo = (ImageView)v.findViewById(R.id.imgv_photo);
		 populateUserProfile();
		
		Resources r = getResources();
		tabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
		tabHost.setup(c, getChildFragmentManager(), R.id.realtabcontent);
	  
		tabHost.addTab(tabHost.newTabSpec("Timeline").setIndicator(null, r.getDrawable(R.drawable.selector_profile_timeline)),
				ProfileTimelineFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("Ring").setIndicator(null, r.getDrawable(R.drawable.selector_profile_ring)),
				ProfileRingFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("Album").setIndicator(null, r.getDrawable(R.drawable.selector_profile_album)),
				ProfileAlbumFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("Buddy").setIndicator(null, r.getDrawable(R.drawable.selector_profile_buddy)),
				ProfileBuddyFragment.class, null);		
		
		return v;
	}

	private void populateUserProfile() {		
		User currentUser = (User) ParseUser.getCurrentUser();	
		tv_rank.setText(currentUser.getRank());
		tv_name.setText(currentUser.getName());
		tv_status.setText(currentUser.getStatus());
		tv_followers.setText(Integer.toString(currentUser.getFollowersCount())+ " followers");
		tv_buddies.setText(Integer.toString(currentUser.getFriendsCount())+" buddies");	
		PhotoHelper pHelper = new PhotoHelper();
		pHelper.processParseFile(currentUser.getPhoto(),imgv_photo);	
	}
}

// Onclicklistener
/*
 * btn_search.setOnClickListener(new OnClickListener(){ public void onClick(View
 * v){
 * 
 * }
 * 
 * });
 */