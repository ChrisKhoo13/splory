package my.splory.fragment;

import my.splory.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class JioComposeFragment extends Fragment{
	private Context c;
	private static final String TAG = "JioComposeFragment";	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		c = getActivity();
		View view = inflater.inflate(R.layout.fragment_jio_compose, container,
				false);
		
		return view;
	}
}
