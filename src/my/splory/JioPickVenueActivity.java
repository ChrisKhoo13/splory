package my.splory;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

public class JioPickVenueActivity extends Activity implements LocationListener{
private ListView lv_places ;
private String requestLink;
private static final String CLIENT_ID ="client_id";
private static final String CLIENT_SECRET ="client_secret";
private static final String VERSION ="v";
private static final String LAT_LONG ="ll";
private static final String SEARCH_TERM ="query";
private static final String TAG = "JioPickVenueActivity";
private int latitude;
private int longitude;
private LocationManager locationManager;
private String provider;
private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_pickvenue);
		context = this;
		lv_places = (ListView)findViewById(R.id.lv_places);
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
	    Location location = locationManager.getLastKnownLocation(provider);

	    // Initialize the location fields
	    if (location != null) {
	      System.out.println("Provider " + provider + " has been selected.");
	      onLocationChanged(location);
	    } else {
	      Toast.makeText(context, getString(R.string.toast_gps_failed), Toast.LENGTH_SHORT).show();
	    }
	  
	    Log.i(TAG, "latitude: "+ Integer.toString(latitude)+" longitude: "+Integer.toString(longitude));
		
	}

	@Override
	public void onLocationChanged(Location location) {
		latitude = (int) (location.getLatitude());
		longitude = (int) (location.getLongitude());
	    
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
}

