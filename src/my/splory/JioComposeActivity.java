package my.splory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import my.splory.subclass.parse.Post;
import my.splory.subclass.parse.User;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class JioComposeActivity extends BaseActivity {
	private TextView tv_jio_buddies_grps;	
	private TextView tv_invitees;
	private TextView tv_when;
	private EditText et_event_name;
	private EditText et_notes;	
	private EditText et_where;
	private ImageButton imgb_jio;
	private static final int PICK_BUDDIIES_REQUEST = 1;
	private static final int PICK_TIME_REQUEST = 2;
	private static final String TAG = "JioComposeActivity";
	private ArrayList<User> inviteesArrList = new ArrayList<User>();
	private String invitees = "";
	private Date date = new Date();

	// To do -
	// Consolidate onClickListener
	// Pass in leftover attributes - checkin, dateFrom, location
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_compose);
		tv_jio_buddies_grps = (TextView) findViewById(R.id.tv_jio_buddies_grps);
		tv_invitees = (TextView) findViewById(R.id.tv_invitees);
		tv_when = (TextView) findViewById(R.id.tv_when);
		imgb_jio = (ImageButton) findViewById(R.id.imgb_jio);
		et_event_name = (EditText) findViewById(R.id.et_event_name);		
		et_notes = (EditText) findViewById(R.id.et_notes);
		et_where = (EditText) findViewById(R.id.et_where);

		tv_jio_buddies_grps.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(JioComposeActivity.this,
						JioPickBuddiesActivity.class);
				startActivityForResult(intent, PICK_BUDDIIES_REQUEST);
			}
		});

		tv_when.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(JioComposeActivity.this,
						JioPickTimeActivity.class);
				startActivityForResult(intent, PICK_TIME_REQUEST);
			}
		});
		
		et_where.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(JioComposeActivity.this,
						JioPickVenueActivity.class);
				startActivity(intent);
			}
		});
		
		// Create a Post ParseObject, Type = Hangout
		imgb_jio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Post hangoutPost = new Post();
				User[] inviteesArr = new User[inviteesArrList.size()];
				inviteesArr = inviteesArrList.toArray(inviteesArr);
				ParseUser currentUser = ParseUser.getCurrentUser();
				hangoutPost.setContent(et_notes.getText().toString());
				hangoutPost.setTaggedContent(et_notes.getText().toString());
				hangoutPost.setType("Hangout");
				hangoutPost.setUserId((User) currentUser);
				hangoutPost.setEventName(et_event_name.getText().toString());
				hangoutPost.setBuddies(inviteesArr);
				hangoutPost.setDateFrom(date);			
				hangoutPost.saveInBackground();			
			
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_BUDDIIES_REQUEST) {
			if (resultCode == RESULT_OK) {
				ArrayList<String> objectIds = data.getStringArrayListExtra("INVITEES");
				for (int i = 0; i < objectIds.size(); i++) {	
					//Use generic function in GeneralFunctions
					ParseQuery<User> query = ParseQuery.getQuery(User.class);
					query.getInBackground(objectIds.get(i),
							new GetCallback<User>() {
								public void done(User result, ParseException e) {
									if (e == null) {
										inviteesArrList.add(result);
										invitees += result.getName() + ", ";
										tv_invitees.setText(invitees);										
									}
								}
							});
				}				
			}			
			else{
				Log.e(TAG, "resultCode: "+ Integer.toString(resultCode));
			}
		}
		else if(requestCode == PICK_TIME_REQUEST){
			if (resultCode == RESULT_OK) {								
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.YEAR, data.getIntExtra("year", 0));
				cal.set(Calendar.MONTH, data.getIntExtra("month", 0));
				cal.set(Calendar.DATE, data.getIntExtra("day", 0));
				cal.set(Calendar.HOUR_OF_DAY, data.getIntExtra("minute", 0));
				cal.set(Calendar.MINUTE, data.getIntExtra("second", 0));
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				date = cal.getTime();
				tv_when.setText(date.toString());
				Log.i(TAG, date.toString());
			}
			else{
				Log.e(TAG, "resultCode: "+ Integer.toString(resultCode));
			}
		}
	}
}
