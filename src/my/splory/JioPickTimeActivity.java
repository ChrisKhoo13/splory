package my.splory;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class JioPickTimeActivity extends Activity{
	private DatePicker datepickerv;
	private TimePicker timepickerv;
	private Button btn_done;
	private static final String TAG = "JioPickTimeActivity";
	private int day;
	private int month;
	private int year;
	private int hour;
	private int min;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_picktime);
		datepickerv = (DatePicker)findViewById(R.id.datepickerv);
		timepickerv = (TimePicker)findViewById(R.id.timepickerv);
		btn_done = (Button)findViewById(R.id.btn_done);
			
		btn_done.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent();  
				intent.putExtra("year", datepickerv.getYear());
				intent.putExtra("month", datepickerv.getMonth());
				intent.putExtra("day", datepickerv.getDayOfMonth());
				intent.putExtra("minute", timepickerv.getCurrentHour());
				intent.putExtra("second", timepickerv.getCurrentMinute());
	            setResult(RESULT_OK, intent);  
	            finish();
				//Log.i(TAG, date.toString());
			}
		});
		
		
	}
}
