package my.splory.helper;

import java.util.ArrayList;

import my.splory.R;
import my.splory.subclass.parse.User;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;

//reuse image processing methods.
//Dynamic displaying the tick. 

//public class PickBuddiesListViewArrayAdapter extends ArrayAdapter<PickBuddiesListViewUser> {
public class ProfileBuddyListViewArrayAdapter extends ArrayAdapter<User> {
	private Context context;
	private int layoutResourceId;
	//private ArrayList<PickBuddiesListViewUser> users = new ArrayList<PickBuddiesListViewUser>();
	private ArrayList<User> users = new ArrayList<User>();
	private static final String TAG = "ProfileBuddyListViewArrayAdapter";
	private ViewHolder holder = new ViewHolder();
	
	public ProfileBuddyListViewArrayAdapter(Context context, int layoutResourceId,
			ArrayList<User> users) {
		super(context, layoutResourceId, users);
		this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.users = users;
        Log.i(TAG, Integer.toString(users.size()));
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder.imgv_pic = (ImageView)convertView.findViewById(R.id.imgv_pic);
			holder.tv_name = (TextView)convertView.findViewById(R.id.tv_name);
			convertView.setTag(holder);
		}
		else{
			 holder = (ViewHolder)convertView.getTag();
		}
		
		//PickBuddiesListViewUser user = users.get(position);		
		User userObj = users.get(position);		
		ParseFile img = userObj.getPhoto();
		
		if(img != null){
			Log.i(TAG, userObj.getName()+ "'s img is not null");
			PhotoHelper pHelper = new PhotoHelper();
			pHelper.processParseFile(img, holder.imgv_pic);	
		}
		holder.tv_name.setText(userObj.getName());		
		return convertView;
	}
	
	private static class ViewHolder {
		ImageView imgv_pic;				
		TextView tv_name;
	}


}
