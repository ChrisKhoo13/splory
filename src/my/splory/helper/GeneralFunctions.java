package my.splory.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import my.splory.subclass.parse.User;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

public class GeneralFunctions {

	private ArrayList<String> name = new ArrayList<String>();
	private String location;
	private static final String TAG = "GeneralFunctions"; 
	
	public ArrayList<String> getParseUserName(ArrayList<String> objectIds) {
		for (int i = 0; i < objectIds.size(); i++) {
			ParseQuery<User> query = ParseQuery.getQuery(User.class);
			query.getInBackground(objectIds.get(i), new GetCallback<User>() {
				public void done(User result, ParseException e) {
					if (e == null) {
						Log.i(TAG, result.getName());
						name.add(result.getName());
					}
					else{
						Log.e(TAG, e.getMessage());
					}
				}
			});
		}
		return name;
	}
	
	public String formatDate(Date date, String pattern){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);		
		String formattedDate = new SimpleDateFormat(pattern).format(cal.getTime());		
		return formattedDate.replace("AM", "am").replace("PM","pm");
	}
	
	public String getLocation(JSONObject locationJson){
		try {				
			location = locationJson.getString("name");
		} catch (JSONException e) {
			//Toast.makeText(context, "JSON error: "+ e.getMessage(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}		
		return location;
	}
}
