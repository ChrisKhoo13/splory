package my.splory.helper;

import java.util.List;

import my.splory.R;
import my.splory.object.Feeds;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomArrayAdapter extends ArrayAdapter<Feeds> {
	private List<Feeds> feeds;
	private LayoutInflater inflater;
	private Context c;

	public enum RowType {
		PHOTO_ITEM, JIO_ITEM, CHECKIN_ITEM
	}

	// public CustomArrayAdapter(Context context, LayoutInflater inflater,
	// List<Feeds> feeds){
	public CustomArrayAdapter(Context c, List<Feeds> feeds) {
		super(c, 0, feeds);
		this.feeds = feeds;
		this.c = c;
	}

	/*
	 * public int getViewTypeCount(){ return RowType.values().length; }
	 */

	public int getItemViewType(int position) {
		return feeds.get(position).getViewType();
	}

	/*
	 * public View getView(int position, View convertView, ViewGroup parent){
	 * return feeds.get(position).getView(inflater, convertView); }
	 */

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		int type = getItemViewType(position);
		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		switch (type) {
		case 0:
			if (convertView == null) {

				convertView = inflater.inflate(R.layout.feed_photo, parent,
						false);
				holder = new ViewHolder();
				holder.imgv_profilepic = (ImageView) convertView
						.findViewById(R.id.imgv_profilepic);
				holder.tv_timestamp = (TextView) convertView
						.findViewById(R.id.tv_timestamp);
				holder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				holder.tv_caption = (TextView) convertView
						.findViewById(R.id.tv_caption);
				holder.imgv_pic = (ImageView) convertView
						.findViewById(R.id.imgv_pic);
				holder.tv_comment_count = (TextView) convertView
						.findViewById(R.id.tv_comment_count);
				holder.tv_comment = (TextView) convertView
						.findViewById(R.id.tv_comment);
				holder.tv_approve_count = (TextView) convertView
						.findViewById(R.id.tv_approve_count);
				holder.tv_approve = (TextView) convertView
						.findViewById(R.id.tv_approve);
				holder.tv_bojio_count = (TextView) convertView
						.findViewById(R.id.tv_bojio_count);
				holder.tv_bojio = (TextView) convertView
						.findViewById(R.id.tv_bojio);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			break;
		case 1:
			if (convertView == null) {
				convertView = inflater
						.inflate(R.layout.feed_jio, parent, false);

				holder = new ViewHolder();
				holder.imgv_profilepic = (ImageView) convertView
						.findViewById(R.id.imgv_profilepic);
				holder.tv_timestamp = (TextView) convertView
						.findViewById(R.id.tv_timestamp);
				holder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				holder.tv_tagged_names = (TextView) convertView
						.findViewById(R.id.tv_tagged_names);
				holder.tv_jio_title = (TextView) convertView
						.findViewById(R.id.tv_jio_title);
				holder.tv_jio_date = (TextView) convertView
						.findViewById(R.id.tv_jio_date);
				holder.tv_comment_count = (TextView) convertView
						.findViewById(R.id.tv_comment_count);
				holder.tv_comment = (TextView) convertView
						.findViewById(R.id.tv_comment);
				holder.tv_approve_count = (TextView) convertView
						.findViewById(R.id.tv_approve_count);
				holder.tv_approve = (TextView) convertView
						.findViewById(R.id.tv_approve);
				holder.tv_bojio_count = (TextView) convertView
						.findViewById(R.id.tv_bojio_count);
				holder.tv_bojio = (TextView) convertView
						.findViewById(R.id.tv_bojio);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			break;
		case 2:
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.feed_checkin, parent,
						false);
				holder = new ViewHolder();
				holder.imgv_profilepic = (ImageView) convertView
						.findViewById(R.id.imgv_profilepic);
				holder.tv_timestamp = (TextView) convertView
						.findViewById(R.id.tv_timestamp);
				holder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				holder.tv_checkin = (TextView) convertView
						.findViewById(R.id.tv_checkin);
				holder.tv_comment_count = (TextView) convertView
						.findViewById(R.id.tv_comment_count);
				holder.tv_comment = (TextView) convertView
						.findViewById(R.id.tv_comment);
				holder.tv_approve_count = (TextView) convertView
						.findViewById(R.id.tv_approve_count);
				holder.tv_approve = (TextView) convertView
						.findViewById(R.id.tv_approve);
				holder.tv_bojio_count = (TextView) convertView
						.findViewById(R.id.tv_bojio_count);
				holder.tv_bojio = (TextView) convertView
						.findViewById(R.id.tv_bojio);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			break;
		}

		// holder.textView.setText(mData.get(position));
		return convertView;
	}

	public static class ViewHolder {
		//header components
		public ImageView imgv_profilepic;
		public TextView tv_timestamp;
		public TextView tv_name;

		//photo feed components
		public TextView tv_caption;
		public ImageView imgv_pic;
		
		//checkin feed components
		public TextView tv_checkin;
		
		//jio feed components
		public TextView tv_tagged_names;
		public TextView tv_jio_title;
		public TextView tv_jio_date;

		//footer component
		public TextView tv_comment_count;
		public TextView tv_approve_count;
		public TextView tv_bojio_count;
		public TextView tv_comment;
		public TextView tv_approve;		
		public TextView tv_bojio;
	}

}
