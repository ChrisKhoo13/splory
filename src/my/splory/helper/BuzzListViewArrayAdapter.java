package my.splory.helper;

import java.util.ArrayList;

import com.parse.ParseFile;

import my.splory.R;
import my.splory.subclass.parse.Buzz;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BuzzListViewArrayAdapter extends ArrayAdapter<Buzz>{
	private Context context;
	private int layoutResourceId;
	private ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
	private static final String TAG = "BuzzListViewArrayAdapter";
	
	
	public BuzzListViewArrayAdapter (Context context, int layoutResourceId, ArrayList<Buzz> buzzes ){
		super(context, layoutResourceId, buzzes);
		this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.buzzes = buzzes;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){	
		ViewHolder holder = new ViewHolder();
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);			
			holder.imgv_pic = (ImageView) convertView.findViewById(R.id.imgv_pic);
			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
			holder.tv_buzz = (TextView) convertView.findViewById(R.id.tv_buzz);
			convertView.setTag(holder);			
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		Buzz buzzObj = buzzes.get(position);		
		holder.tv_buzz.setText(buzzObj.getMessage());
		ParseFile img = buzzObj.getSenderPhoto();
		if(img != null){
			PhotoHelper pHelper = new PhotoHelper();
			pHelper.processParseFile(img, holder.imgv_pic);	
		}
		return convertView;
	}
	
	private static class ViewHolder{
		ImageView imgv_pic;
		TextView tv_time;
		TextView tv_buzz;
	}
	
}
