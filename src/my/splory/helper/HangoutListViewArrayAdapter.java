package my.splory.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import my.splory.R;
import my.splory.subclass.parse.Post;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class HangoutListViewArrayAdapter extends ArrayAdapter<Post> {
	private Context context;
	private int layoutResourceId;
	private ArrayList<Post> hangouts = new ArrayList<Post>();
	private String locationName = "";
	private static final String TAG = "HangoutListViewArrayAdapter";
	
	
	public HangoutListViewArrayAdapter (Context context, int layoutResourceId, ArrayList<Post> hangouts ){
		super(context, layoutResourceId, hangouts);
		this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.hangouts = hangouts;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder = new ViewHolder();
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);			
			holder.tv_event = (TextView) convertView.findViewById(R.id.tv_event);
			holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
			holder.tv_venue = (TextView) convertView.findViewById(R.id.tv_venue);
			holder.tv_toggle = (TextView) convertView.findViewById(R.id.tv_toggle);
			convertView.setTag(holder);			
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		Post hangout = hangouts.get(position);
		GeneralFunctions func = new GeneralFunctions();	
	
		
		holder.tv_date.setText(func.formatDate(hangout.getDateFrom(), "dd MMM, ccc. h a"));		
		holder.tv_venue.setText(func.getLocation(hangout.getLocation()));
		holder.tv_event.setText(hangout.getEventName());
		return convertView;
	}
	
	private static class ViewHolder{
		TextView tv_event;
		TextView tv_date;
		TextView tv_venue;
		TextView tv_toggle;
	}
}
