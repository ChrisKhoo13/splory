package my.splory.helper;

import my.splory.fragment.ExploreFragment;
import my.splory.fragment.FeedsFragment;
import my.splory.fragment.JioFragment;
import my.splory.fragment.ProfileFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {
	 
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:            
            return new FeedsFragment();
        case 1:
            return new ExploreFragment();
        case 2:
            return new JioFragment();
        case 3:
            return new ProfileFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
 
}