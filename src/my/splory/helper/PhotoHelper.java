package my.splory.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.widget.ImageView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

public class PhotoHelper {
	private static final String TAG = "PhotoHelper";
	
	public void processParseFile(ParseFile imageFile, final ImageView imageView){
		Log.i(TAG, "The img is "+imageFile.toString());
		imageFile.getDataInBackground(new GetDataCallback(){
		@Override
		public void done(byte[] abyte0, ParseException parseexception) {
			if(parseexception == null){				
				Bitmap bitmap = BitmapFactory.decodeByteArray(abyte0 , 0, abyte0.length);
				imageView.setImageBitmap(getRoundedShape(bitmap));
			}
			else{
				Log.e(TAG, parseexception.getMessage());
			}
		}
		
	});
	}	
	
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 100;
		int targetHeight = 100;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth) / 2, ((float) targetHeight) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CW);
		Paint paint = new Paint();
		paint.setColor(Color.GRAY);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStyle(Paint.Style.FILL);
		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setFilterBitmap(true);
		canvas.drawOval(new RectF(0, 0, targetWidth, targetHeight), paint);
		// paint.setColor(Color.TRANSPARENT);
		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new RectF(0, 0, targetWidth,
				targetHeight), paint);
		return targetBitmap;
	}
}
