package my.splory;

import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
//Yc came
public class BaseActivity extends ActionBarActivity {
	private TextView tv_buzz_count;
	private String buzzCount;
	private ActionBar actionBar;
	private static final String TAG = "BaseActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 actionBar = getActionBar();
		 actionBar.setHomeButtonEnabled(true);
		// Customize action bar		
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//	    actionBar.setCustomView(R.layout.actionbar);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		// Inflate Buzz's action view and set the buzz's count in background thread.
		RelativeLayout buzzLayout = (RelativeLayout) menu.findItem(
				R.id.action_buzz).getActionView();
		buzzLayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(BaseActivity.this, BuzzActivity.class);
				startActivity(i);
			}
		});
		tv_buzz_count = (TextView) buzzLayout.findViewById(R.id.tv_buzz_count);
		getBuzzCount();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {			
			Intent i = new Intent(BaseActivity.this, SettingsActivity.class);
			startActivity(i);			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// We will decide if we have to create a helper class for this in the later
	// phase (for cleaner code and less redundancy)
	private String getBuzzCount() {
		buzzCount = "0";
		Map<String, String> emptyMap = new HashMap<String, String>();
		ParseCloud.callFunctionInBackground(getString(R.string.get_buzz_count), emptyMap,
				new FunctionCallback<Integer>() {
					public void done(Integer result, ParseException e) {
						if (e == null) {
							buzzCount = result.toString();
						} else {
							Log.e(TAG, e.toString());
						}
						tv_buzz_count.setText(buzzCount);
					}
				});
		return buzzCount;
	}
}
