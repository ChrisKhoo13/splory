package my.splory.subclass.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Comment")
public class Comment extends ParseObject{
	private String message;
	private Post postId;
	private String taggedMessage;
	private String type;
	private User userId;
}
