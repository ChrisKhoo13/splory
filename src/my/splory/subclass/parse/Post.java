package my.splory.subclass.parse;

import java.util.Arrays;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseRelation;


@ParseClassName("Post")
public class Post extends ParseObject{
	//Clean up the unused declarations
	//Number or int
	//createdAt, updatedAt
	private String objectId;
	private String content;
	private String type;
	private ParseRelation<User> approve;
	private ParseRelation<User> bojio;	
	private int approveCount;
	private int bojioCount;
	private JSONArray buddies; //Buddies tagged
	private Object checkin;
	private ParseRelation<Comment> comments;
	private int commentsCount;
	private JSONArray photo;
	private String taggedContent;
	private User userId;
	private ParseRelation<User> RSVP;
    private int RSVPCount;
    private Date dateFrom;
    private String eventName;
    private Object location;    
	
	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return getString("objectId");
	}
	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		put("objectId", objectId);
	}
	
	
	
	/**
	 * @return the content
	 */
	public String getContent() {
		return getString("content");
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		put("content", content);
	}
	
	
	
	/**
	 * @return the type
	 */
	public String getType() {
		return getString("type");
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		put("type", type);
	}
	
	
	
	/**
	 * @return the approve
	 */
	public ParseRelation<User> getApprove() {		
		return getRelation("approve");
	}
	/**
	 * @param approve the approve to set
	 */
	public void setApprove(User approve) {
		getApprove().add(approve);
	}
	
	
	
	/**
	 * @return the approveCount
	 */
	public int getApproveCount() {
		return getInt("approveCount");
	}
	/**
	 * @param approveCount the approveCount to set
	 */
	public void setApproveCount(int approveCount) {
		put("approveCount", approveCount);
	}
	
	
	
	/**
	 * @return the bojio
	 */
	public ParseRelation<User> getBojio() {
		return getRelation("bojio");
	}
	/**
	 * @param bojio the bojio to set
	 */
	public void setBojio(User bojio) {
		getBojio().add(bojio);
	}
	
	
	
	/**
	 * @return the bojioCount
	 */
	public int getBojioCount() {
		return getInt("bojioCount");
	}
	/**
	 * @param bojioCount the bojioCount to set
	 */
	public void setBojioCount(int bojioCount) {
		put("bojioCount", bojioCount);
	}
	
	
	
	/**
	 * @return the buddies
	 */	
	public JSONArray getBuddies() {		
		return getJSONArray("buddies");
	}
	/**
	 * @param buddies the buddies to set
	 */
	public void setBuddies(User[] buddies) {
		put("buddies", Arrays.asList(buddies));
	}
	
	
	
	/**
	 * @return the checkin
	 */
	public Object getCheckin() {
		return get("checkin");
	}
	/**
	 * @param checkin the checkin to set
	 */
	public void setCheckin(Object checkin) {
		put("checkin", checkin);
	}
	
	
	
	/**
	 * @return the comments
	 */
	public ParseRelation<Comment> getComments() {
		return getRelation("comments");
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(Comment comments) {
		getComments().add(comments);
	}
	
	
	
	/**
	 * @return the commentsCount
	 */
	public int getCommentsCount() {
		return getInt("commentsCount");
	}
	/**
	 * @param commentsCount the commentsCount to set
	 */
	public void setCommentsCount(int commentsCount) {
		put("commentsCount", commentsCount);
	}
	
	
	
	/**
	 * @return the photo
	 */
	public ParseFile[] getPhoto() {
		return (ParseFile[]) get("photos");
	}
	/**
	 * @param photos the photo to set
	 */
	public void setPhoto(ParseFile[] photo) {
		put("photos", Arrays.asList(photo));
	}
	
	
	
	/**
	 * @return the taggedContent
	 */
	public String getTaggedContent() {
		return getString("taggedContent");
	}
	/**
	 * @param taggedContent the taggedContent to set
	 */
	public void setTaggedContent(String taggedContent) {
		put("taggedContent", taggedContent);
	}
	
	
	
	/**
	 * @return the userId
	 */
	public User getUserId() {
		return (User)get("userId");
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(User userId) {
		put("userId", userId);
	}
	
	
	
	/**
	 * @return the RSVP
	 */
	public ParseRelation<User> getRSVP() {
		return getRelation("RSVP");
	}
	/**
	 * @param rSVP the RSVP to set
	 */
	public void setRSVP(User RSVP) {
		getRSVP().add(RSVP);
	}
	
	
	
	/**
	 * @return the RSVPCount
	 */
	public int getRSVPCount() {
		return getInt("RSVPCount");
	}
	/**
	 * @param RSVPCount the RSVPCount to set
	 */
	public void setRSVPCount(int RSVPCount) {
		put("RSVPCount", RSVPCount);
	}
	
	
	
	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return getDate("dateFrom");
	}
	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(Date dateFrom) {
		put("dateFrom", dateFrom);
	}
	
	
	
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return getString("eventName");
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		put("eventName", eventName);
	}
	
	
	
	/**
	 * @return the location
	 */
	public JSONObject getLocation() {
		return getJSONObject("location");
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(JSONObject location) {
		put("location", location);
	}
}
