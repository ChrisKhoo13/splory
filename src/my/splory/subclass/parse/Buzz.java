package my.splory.subclass.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("Buzz")
public class Buzz extends ParseObject{
	private User userId;
	private String toWho;
	private String targetObjectId;
	private String type;
	private String message;
	private Boolean read;
	/*private User sender;*/
	private ParseFile senderPhoto;
	public Buzz(){
		
	}

	public User getUserId() {		
		return (User)getParseObject("userId");
	}
	public void setUserId(User userId) {
		put("userId", userId);
		//Set Sender's photo 
		//sender = (User)getParseUser(userId);
		setSenderPhoto(userId.getPhoto());
	}
	
	public String getToWho() {
		return getString("toWho");
	}
	public void setToWho(String toWho) {
		put("toWho", toWho);
	}
	public String getTargetObjectId() {
		return getString("targetObjectId");
	}
	public void setTargetObjectId(String targetObjectId) {
		put("targetObjectId", targetObjectId);
	}
	public String getType() {
		return getString("type");
	}
	public void setType(String type) {
		put("type", type);
	}
	public String getMessage() {
		return getString("message");
	}
	public void setMessage(String message) {
		put("message", message);
	}
	public Boolean getRead() {
		return getBoolean("read");
	}
	public void setRead(Boolean read) {
		put("read", read);
	}
	/**
	 * @return the senderPhoto
	 */
	public ParseFile getSenderPhoto() {
		return senderPhoto;
	}
	/**
	 * @param senderPhoto the senderPhoto to set
	 */
	public void setSenderPhoto(ParseFile senderPhoto) {
		this.senderPhoto = senderPhoto;
	}
		
}
