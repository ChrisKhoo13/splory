package my.splory.subclass.parse;

import java.io.File;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseRelation;
import com.parse.ParseUser;

@ParseClassName("_User")
public class User extends ParseUser {
	private String name;
	private String fbuid;
	private String status;
	private String title;
	private String rank;
	private ParseFile photo;
	private int followers;
	private int buddies;
	private boolean selected = false;
	
	public User(){
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return getString("name");
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		put("name", name);		
	}

	/**
	 * @return the fbuid
	 */
	public String getFbuid() {
		return getString("fbuid");
	}

	/**
	 * @param fbuid the fbuid to set
	 */
	public void setFbuid(String fbuid) {
		put("fbuid", fbuid);
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return getString("status");
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		put("status", status);
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return getString("title");
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		put("title", title);
	}

	/**
	 * @return the rank
	 */
	public String getRank() {
		return getString("rank");
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(String rank) {
		put("rank", rank);
	}

	/**
	 * @return the photo
	 */
	public ParseFile getPhoto() {
		return getParseFile("photo");
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(File photo) {
		put("photo", photo);
	}

	/**
	 * @return the followersCount
	 */
	public int getFollowersCount() {
		return getInt("followersCount");
	}

	/**
	 * @param followersCount the followersCount to set
	 */
	public void setFollowersCount(int followersCount) {
		put("followersCount", followersCount);
	}

	/**
	 * @return the friendsCount
	 */
	public int getFriendsCount() {
		return getInt("friendsCount");
	}

	/**
	 * @param friendsCount the friendsCount to set
	 */
	public void setFriendsCount(int friendsCount) {
		put("friendsCount", friendsCount);
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	/**
	 * @return the friends
	 */
	public ParseRelation<User> getFriends() {
		return getRelation("friends");
	}
	/**
	 * @param friend the friend to set
	 */
	public void setFriends(User friend) {
		getFriends().add(friend);
	}
		
	/* public static final Parcelable.Creator<User> CREATOR = new Creator<User>() {  
		 public User createFromParcel(Parcel source) {  
			 User mBook = new User();  
		     mBook.name = source.readString();  
		     mBook.fbuid = source.readString();  
		     mBook.status = source.readString();  
		     mBook.title = source.readString();  
		     mBook.rank = source.readString();  
		     mBook.photo = source.read  
		     mBook.followers = source.readInt();  
		     mBook.buddies = source.readInt();  
		     mBook.selected = source.readInt();  
		     return mBook;  
		 }  
		 public Book[] newArray(int size) {  
		     return new Book[size];  
		 }  
		    };  
		       
		    public int describeContents() {  
		 return 0;  
		    }  
		    public void writeToParcel(Parcel parcel, int flags) {  
		 parcel.writeString(bookName);  
		 parcel.writeString(author);  
		 parcel.writeInt(publishTime);  
		    } */ 
	
	
}
