package my.splory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import my.splory.helper.BuzzListViewArrayAdapter;
import my.splory.subclass.parse.Buzz;
import my.splory.subclass.parse.User;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

public class BuzzActivity extends BaseActivity {
	private ListView listview_buzz;
	private ActionBar actionBar ; 
	private static final String TAG = "BuzzActivity";
	private Context context;
	private ArrayList<Buzz> buzzes = new ArrayList<Buzz>();
	private static final String BUDDY_REQUEST = "buddy_request";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buzz);
		listview_buzz = (ListView) findViewById(R.id.listview_buzz);		
		context = this;
		popListView();
		Log.d(TAG, "onCreate");
		
		listview_buzz.setOnItemClickListener(new OnItemClickListener() {
			@Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				 Buzz buzzObj = (Buzz)parent.getItemAtPosition(position);
				 //If Buzz is a buddy request  
				 Log.i(TAG, buzzObj.getType());
				 Log.i(TAG, buzzObj.getUserId().getName());
				 if(buzzObj.getType().contains(BUDDY_REQUEST)){
					 	Map<String, String> budRequestSenderMap = new HashMap<String, String>();
					 	budRequestSenderMap.put("who", buzzObj.getUserId().getObjectId());
						ParseCloud.callFunctionInBackground("AcceptBuddyRequest", budRequestSenderMap, new FunctionCallback<Map<String, String>>() {
							public void done(Map<String, String> results, ParseException e) {
								 if(e == null){
									 Log.i(TAG, "Done");	 
									 Toast.makeText(getApplicationContext(), "Buddy Request accepted - sending buzz out ", Toast.LENGTH_LONG).show();
								 }
								 else{
									 Log.e(TAG, e.getMessage());	 
								 }							 
								 
							}});				 
				 }
				 
			}
			
		});
	}

	
	private void popListView() {		
	 	Map<String, String> emptyMap = new HashMap<String, String>();
		ParseCloud.callFunctionInBackground("GetBuzz", emptyMap, new FunctionCallback<ArrayList<Buzz>>() {
			      public void done(ArrayList<Buzz> results, ParseException e) {
			        if (e == null) {			        	
			        	for(int i=0 ; i<results.size() ; i++){			        		
			        		Buzz buzzObj = new Buzz();
			        		Log.i(TAG, results.get(i).getMessage());
			        		//buzzObj needs 4 fields : createdAt , message , user id's photo, type
			        		buzzObj.setMessage(results.get(i).getMessage());
			        		buzzObj.setUserId(results.get(i).getUserId());
			        		buzzObj.setSenderPhoto(results.get(i).getSenderPhoto());
			        		buzzObj.setType(results.get(i).getType());
			        		buzzes.add(buzzObj);
			        	}
			          
			        } else {
			        	 Log.d(TAG, e.toString());
			        }
			        BuzzListViewArrayAdapter adapter = new BuzzListViewArrayAdapter(context, R.layout.listview_row_layout_buzz, buzzes);
					listview_buzz.setAdapter(adapter);		
			      }
			 });
			 
			
		}
}
