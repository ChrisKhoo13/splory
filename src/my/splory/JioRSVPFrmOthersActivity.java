package my.splory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import my.splory.fragment.JioFragment;
import my.splory.subclass.parse.User;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;

public class JioRSVPFrmOthersActivity extends Activity{
	private TextView tv_from;
	private TextView tv_invitees;
	private TextView tv_event;
	private TextView tv_time;
	private TextView tv_venue;
	private TextView tv_notes;
	private Switch switch_rsvp;
	private ArrayList<String> inviteesObjId = new ArrayList<String>();
	private String invitees ="";
	private Context context;
	private static final String TAG = "JioRSVPFrmOthersActivity"; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jio_rsvp_frm_others);
		context = this;
		tv_from = (TextView) findViewById(R.id.tv_from);
		tv_invitees = (TextView) findViewById(R.id.tv_invitees);
		tv_event = (TextView) findViewById(R.id.tv_event);
		tv_time = (TextView) findViewById(R.id.tv_time);
		tv_venue = (TextView) findViewById(R.id.tv_venue);
		tv_notes = (TextView) findViewById(R.id.tv_notes);
		switch_rsvp = (Switch) findViewById(R.id.switch_rsvp);
				
		Intent intent = getIntent();
		inviteesObjId = intent.getStringArrayListExtra(JioFragment.HANGOUT_INVITEES);		
		for(int i=0; i<inviteesObjId.size(); i++){
			ParseQuery<User> query = ParseQuery.getQuery(User.class);
			query.getInBackground(inviteesObjId.get(i),
					new GetCallback<User>() {
						public void done(User result, ParseException e) {
							if (e == null) {
								invitees += result.getName() + ", ";
								tv_invitees.setText(invitees);							
							}
						}
					});
		}		
		
		tv_from.setText("from "+ intent.getStringExtra(JioFragment.HANGOUT_FROM));
		tv_event.setText(intent.getStringExtra(JioFragment.HANGOUT_NAME));
		tv_time.setText(intent.getStringExtra(JioFragment.HANGOUT_TIME));
		tv_venue.setText(intent.getStringExtra(JioFragment.HANGOUT_VENUE));
		tv_notes.setText(intent.getStringExtra(JioFragment.HANGOUT_NOTES));		
		
		switch_rsvp.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		     Map<String, String> rsvpMap = new HashMap<String, String>();	
		       if(isChecked){		   
				    //rsvpMap.put("hangoutId", buzzObj.getUserId().getObjectId());	
				    //rsvpMap.put("coming", "true");
		   			/*ParseCloud.callFunctionInBackground("RSVPHangout", budRequestSenderMap, new FunctionCallback<Map<String, String>>() {
					public void done(Map<String, String> results, ParseException e) {
						 if(e == null){
							 Log.i(TAG, "Done");	 
							 Toast.makeText(getApplicationContext(), "Buddy Request accepted - sending buzz out ", Toast.LENGTH_LONG).show();
						 }
						 else{
							 Log.e(TAG, e.getMessage());	 
						 }							 
						 
					}});*/
		    	   
		    	   
		    	   Toast.makeText(context, "checked", Toast.LENGTH_SHORT).show();
		       }
		       else{
		    	   Toast.makeText(context, "not checked", Toast.LENGTH_SHORT).show();
		       }
		    }
		});
		
	}
}
