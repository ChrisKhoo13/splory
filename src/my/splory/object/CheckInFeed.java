package my.splory.object;

import my.splory.R;
import my.splory.helper.CustomArrayAdapter.RowType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class CheckInFeed implements Feeds{
	    private String name;
	    private String venue;
	    private String date;
	    private String type;
	 
	    public CheckInFeed (String name, String date, String venue, String type) {
	        this.name = name;
	        this.venue = venue;
	        this.date = date;
	        this.type = type;
	    }
	    
	    @Override
	    public String getType()
	    {
	    	return type;
	    }
	 
	    @Override
	    public int getViewType() {
	        return RowType.CHECKIN_ITEM.ordinal();
	    }
	 
	   /* @Override
	    public View getView(LayoutInflater inflater, View convertView) {
	        if (convertView == null) {
	            convertView = (View) inflater.inflate(R.layout.feed_checkin, null);
	        }
	 
	        TextView text1 = (TextView) convertView.findViewById(R.id.name);
	        TextView text2 = (TextView) convertView.findViewById(R.id.date);
	        TextView text3 = (TextView) convertView.findViewById(R.id.venue);
	        text1.setText(name);
	        text2.setText(date);
	        text3.setText(venue);
	 
	        return convertView;
	    }*/
}
