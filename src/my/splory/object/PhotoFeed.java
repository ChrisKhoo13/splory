package my.splory.object;

import my.splory.R;
import my.splory.helper.CustomArrayAdapter.RowType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class PhotoFeed implements Feeds {
	private String name;
	private String date;
	private String post;
	private String type;

	public PhotoFeed(String name, String date, String post, String type) {
		this.name = name;
		this.date = date;
		this.post = post;
		this.type = type;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public int getViewType() {
		return RowType.PHOTO_ITEM.ordinal();
	}

	/*@Override
	public View getView(LayoutInflater inflater, View convertView) {
		if (convertView == null) {
			// No views to reuse, need to inflate a new view
			convertView = (View) inflater.inflate(R.layout.feed_photo, null);
		}

		TextView text1 = (TextView) convertView.findViewById(R.id.name);
		TextView text2 = (TextView) convertView.findViewById(R.id.date);
		TextView text3 = (TextView) convertView.findViewById(R.id.post);

		text1.setText(name);
		text2.setText(date);
		text3.setText(post);

		return convertView;
	}*/
}
