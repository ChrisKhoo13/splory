package my.splory.object;

import android.view.LayoutInflater;
import android.view.View;

public interface Feeds {
	public String getType();
	public int getViewType();
	//public View getView(LayoutInflater inflater, View convertView);
}