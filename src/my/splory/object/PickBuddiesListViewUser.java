package my.splory.object;

import com.parse.ParseFile;


public class PickBuddiesListViewUser {
private String name;
private ParseFile photo;
private boolean selected;
/**
 * @return the name
 */
public String getName() {
	return name;
}
/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
}
/**
 * @return the photo
 */
public ParseFile getPhoto() {
	return photo;
}
/**
 * @param photo the photo to set
 */
public void setPhoto(ParseFile photo) {
	this.photo = photo;
}
/**
 * @return the selected
 */
public boolean isSelected() {
	return selected;
}
/**
 * @param selected the selected to set
 */
public void setSelected(boolean selected) {
	this.selected = selected;
}

}
